package collector

import (
	"gitlab.com/golibs/crawler/logs"
)

var (
	// 全局支持的输出方式
	DataOutput = make(map[string]func(self *Collector) error)

	// 全局支持的文本Data output方式名称列表
	DataOutputLib []string
)

// 文本Data output
func (self *Collector) outputData() {
	defer func() {
		// 回收缓存块
		self.resetDataDocker()
	}()

	// 输出
	dataLen := uint64(len(self.DataDocker))
	if dataLen == 0 {
		return
	}

	defer func() {
		if p := recover(); p != nil {
			logs.Log.Informational(" * ")
			logs.Log.App("[SYS] Panic  [Data output：%v | KEYIN：%v | Batch：%v]   Data %v ! [ERROR]  %v\n",
				self.Spider.GetName(), self.Spider.GetKeyin(), self.dataBatch, dataLen, p)
		}
	}()

	// 输出统计
	self.addDataSum(dataLen)

	// 执行输出
	err := DataOutput[self.outType](self)

	logs.Log.Informational(" * ")
	if err != nil {
		logs.Log.App("[SYS] Fail  [Data output：%v | KEYIN：%v | Batch：%v]   Data %v ! [ERROR]  %v\n",
			self.Spider.GetName(), self.Spider.GetKeyin(), self.dataBatch, dataLen, err)
	} else {
		logs.Log.App("[SYS] [Data output：%v | KEYIN：%v | Batch：%v]   Data %v !\n",
			self.Spider.GetName(), self.Spider.GetKeyin(), self.dataBatch, dataLen)
		self.Spider.TryFlushSuccess()
	}
}
