package spider

import (
	"bytes"
	"io"
	"io/ioutil"
	"mime"
	"net/http"
	"strings"
	"sync"
	"golang.org/x/net/html/charset"

	"gitlab.com/golibs/crawler/app/downloader/request"
	"github.com/tidwall/gjson"
	"gitlab.com/golibs/crawler/common/goquery"
	"gitlab.com/golibs/crawler/common/util"
	"gitlab.com/golibs/crawler/logs"
	"encoding/json"
)

type SingleContext struct {
	spider *Spider // 规则
	//Request   *request.Request // 原始请求
	//Response  *http.Response   // 响应流，其中URL拷贝自*request.Request
	text            []byte // 下载内容Body的字节流格式
	jsonParse       *gjson.Result
	dom             *goquery.Document // 下载内容Body为html时，可转换为Dom的对象
	err             error             // 错误标记
	sync.Mutex
	requestHeaders  http.Header
	responseHeaders http.Header
	statusCode      int
	requestUrl      string
	requestHost     string
	requestMethod   string
}

//**************************************** 初始化 *******************************************\\

func GetSingleContext(sp *Spider, req *request.Request, resp *http.Response, err error) SingleContext {

	context := SingleContext{
		spider: sp,
		//Request:  req.Copy(),
		//Response: resp,
		err:             err,
		requestHeaders:  req.Header,
		statusCode:      resp.StatusCode,
		requestUrl:      req.GetUrl(),
		responseHeaders: resp.Header,
		requestMethod:   resp.Request.Method,
		requestHost:     resp.Request.URL.Host,
	}
	context.initText(req, resp)
	return context

}

//**************************************** Get 类公开方法 *******************************************\\

// 获取下载错误。
func (self *SingleContext) GetError() error {
	self.spider.tryPanic()
	return self.err
}

func (*SingleContext) Log() logs.Logs {
	return logs.Log
}

//func (self *SingleContext) GetResponse() *http.Response {
//	return self.Response
//}

func (self *SingleContext) GetStatusCode() int {
	return self.statusCode
}

func (self *SingleContext) GetKeyin() string {
	return self.spider.GetKeyin()
}

func (self *SingleContext) GetLimit() int {
	return int(self.spider.GetLimit())
}

func (self *SingleContext) GetName() string {
	return self.spider.GetName()
}

//func (self *SingleContext) GetRules() map[string]*Rule {
//	return self.spider.GetRules()
//}
//
//func (self *SingleContext) GetRule(ruleName string) (*Rule, bool) {
//	return self.spider.GetRule(ruleName)
//}
//
//func (self *SingleContext) GetRuleName() string {
//	return self.Request.GetRuleName()
//}

func (self *SingleContext) GetUrl() string {
	return self.requestUrl
}

func (self *SingleContext) GetMethod() string {
	return self.requestMethod
}

func (self *SingleContext) GetHost() string {
	return self.requestHost
}

// 获取响应头信息。
func (self *SingleContext) GetHeader() http.Header {
	return self.responseHeaders
}

// 获取请求头信息。
func (self *SingleContext) GetRequestHeader() http.Header {
	return self.requestHeaders
}

func (self *SingleContext) GetReferer() string {
	return self.responseHeaders.Get("Referer")
}

// 获取响应的Cookie。
func (self *SingleContext) GetCookie() string {
	return self.responseHeaders.Get("Set-Cookie")
}

// GetHtmlParser returns goquery object binded to target crawl result.
func (self *SingleContext) GetDom() *goquery.Document {
	if self.dom == nil {
		self.initDom()
	}
	return self.dom
}

// GetBodyStr returns plain string crawled.
func (self *SingleContext) GetText() string {
	return util.Bytes2String(self.text)
}

// GetBodyStr returns plain string crawled.
func (self *SingleContext) GetJson() *gjson.Result {
	if self.jsonParse == nil {
		parse := gjson.Parse(self.GetText())
		self.jsonParse = &parse
	}
	return self.jsonParse
}
func (self *SingleContext) ParseJson(json string) gjson.Result {
	return gjson.Parse(json)
}

func (self *SingleContext) UnmarshalJson(text string, v interface{}) error {
	return json.Unmarshal([]byte(text), v)
}

// GetHtmlParser returns goquery object binded to target crawl result.
func (self *SingleContext) initDom() *goquery.Document {
	var err error
	self.dom, err = goquery.NewDocumentFromReader(bytes.NewReader(self.text))
	if err != nil {
		panic(err.Error())
	}
	return self.dom
}

// GetBodyStr returns plain string crawled.
func (self *SingleContext) initText(req *request.Request, resp *http.Response) {
	var err error

	// 采用surf内核下载时，尝试自动转码
	if req.DownloaderID == request.SURF_ID {
		var contentType, pageEncode string
		// 优先从响应头读取编码类型
		contentType = resp.Header.Get("Content-Type")
		if _, params, err := mime.ParseMediaType(contentType); err == nil {
			if cs, ok := params["charset"]; ok {
				pageEncode = strings.ToLower(strings.TrimSpace(cs))
			}
		}
		// 响应头未指定编码类型时，从请求头读取
		if len(pageEncode) == 0 {
			contentType = req.Header.Get("Content-Type")
			if _, params, err := mime.ParseMediaType(contentType); err == nil {
				if cs, ok := params["charset"]; ok {
					pageEncode = strings.ToLower(strings.TrimSpace(cs))
				}
			}
		}

		switch pageEncode {
		// 不做转码处理
		case "utf8", "utf-8", "unicode-1-1-utf-8":
		default:
			// 指定了编码类型，但不是utf8时，自动转码为utf8
			// get converter to utf-8
			// Charset auto determine. Use golang.org/x/net/html/charset. Get response body and change it to utf-8
			var destReader io.Reader

			if len(pageEncode) == 0 {
				destReader, err = charset.NewReader(resp.Body, "")
			} else {
				destReader, err = charset.NewReaderLabel(pageEncode, resp.Body)
			}

			if err == nil {
				self.text, err = ioutil.ReadAll(destReader)
				if err == nil {
					resp.Body.Close()
					return
				} else {
					logs.Log.Warning("[SYS] [convert][%v]: %v (ignore transcoding)\n", self.GetUrl(), err)
				}
			} else {
				logs.Log.Warning("[SYS] [convert][%v]: %v (ignore transcoding)\n", self.GetUrl(), err)
			}
		}
	}

	self.text, err = ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		self.err = err
		return
	}

}
