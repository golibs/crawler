package proxy

import (
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"sort"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/golibs/crawler/app/downloader/request"
	"gitlab.com/golibs/crawler/app/downloader/surfer"
	"gitlab.com/golibs/crawler/common/ping"
	"gitlab.com/golibs/crawler/config"
	"gitlab.com/golibs/crawler/logs"
)

type Proxy struct {
	ipRegexp    *regexp.Regexp
	proxyRegexp *regexp.Regexp
	allIps      map[string]string
	all         map[string]bool
	online      int64
	usable      map[string]*ProxyForHost
	ticker      *time.Ticker
	tickSecond  int64
	threadPool  chan bool
	surf        surfer.Surfer
	sync.Mutex
}

const (
	CONN_TIMEOUT = 4 //4s
	DAIL_TIMEOUT = 4 //4s
	TRY_TIMES    = 3
	// IP测速的最大并发量
	MAX_THREAD_NUM = 1000
)

func New() *Proxy {
	p := &Proxy{
		ipRegexp:    regexp.MustCompile(`[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+`),
		proxyRegexp: regexp.MustCompile(`http[s]?://(([^:/]+)?:(([^:/]+)@))?([a-z-.0-9]+):[0-9]+`),
		allIps:      map[string]string{},
		all:         map[string]bool{},
		usable:      make(map[string]*ProxyForHost),
		threadPool:  make(chan bool, MAX_THREAD_NUM),
		surf:        surfer.New(),
	}
	go p.Update()
	return p
}

func (self *Proxy) Count() int64 {
	return self.online
}
func (self *Proxy) FindIp(proxy string) string {

	ip := self.ipRegexp.FindString(proxy)
	if ip != "" {
		return ip
	}
	matchs := self.proxyRegexp.FindStringSubmatch(proxy)
	return matchs[5]

}
func (self *Proxy) Update() *Proxy {
	f, err := os.Open(config.PROXY)
	if err != nil {
		logs.Log.Error("Error: %v", err)
		return self
	}
	b, _ := ioutil.ReadAll(f)
	f.Close()

	proxys := self.proxyRegexp.FindAllString(string(b), -1)
	for _, proxy := range proxys {
		self.allIps[proxy] = self.FindIp(proxy)
		self.all[proxy] = true
	}
	self.online = int64(len(self.all))
	logs.Log.Informational("[SYS] Proxy IP: %v", len(self.all))

	//self.findOnline()

	return self
}

// 筛选在线的代理IP
func (self *Proxy) findOnline() *Proxy {
	logs.Log.Informational("[SYS] Filtering online proxy IP ...")
	self.online = 0
	for proxy := range self.all {
		self.threadPool <- true
		go func(proxy string) {
			ip := self.allIps[proxy]
			logs.Log.Informational("Start ping to proxy=%v ", ip)
			alive, _, _ := ping.Ping(ip, CONN_TIMEOUT)
			self.Lock()
			self.all[proxy] = alive
			self.Unlock()
			if alive {
				atomic.AddInt64(&self.online, 1)
			}
			<-self.threadPool
		}(proxy)
	}
	for len(self.threadPool) > 0 {
		time.Sleep(0.2e9)
	}
	self.online = atomic.LoadInt64(&self.online)
	logs.Log.Informational("[SYS] Total online proxy：%v", self.online)

	return self
}

// 更新继时器
func (self *Proxy) UpdateTicker(tickSecond int64) {
	self.tickSecond = tickSecond
	self.ticker = time.NewTicker(time.Duration(self.tickSecond) * time.Second)
	for _, proxyForHost := range self.usable {
		proxyForHost.curIndex++
		proxyForHost.isEcho = true
	}
}

// 获取本次循环中未使用的代理IP及其响应时长
func (self *Proxy) GetOne(u string) (curProxy string) {
	if self.online == 0 {
		return
	}
	u2, _ := url.Parse(u)
	if u2.Host == "" {
		logs.Log.Informational("[SYS] [%v]Set proxy IP failed, the target url is incorrect", u)
		return
	}
	var key = u2.Host
	if strings.Count(key, ".") > 1 {
		key = key[strings.Index(key, ".")+1:]
	}

	self.Lock()
	defer self.Unlock()

	var ok = true
	var proxyForHost = self.usable[key]

	select {
	case <-self.ticker.C:
		proxyForHost.curIndex++
		if proxyForHost.curIndex >= proxyForHost.Len() {
			_, ok = self.testAndSort(key, u2.Scheme+"://"+u2.Host)
		}
		proxyForHost.isEcho = true

	default:
		if proxyForHost == nil {
			self.usable[key] = &ProxyForHost{
				proxys:    []string{},
				timedelay: []time.Duration{},
				isEcho:    true,
			}
			proxyForHost, ok = self.testAndSort(key, u2.Scheme+"://"+u2.Host)
		} else if l := proxyForHost.Len(); l == 0 {
			ok = false
		} else if proxyForHost.curIndex >= l {
			_, ok = self.testAndSort(key, u2.Scheme+"://"+u2.Host)
			proxyForHost.isEcho = true
		}
	}
	if !ok {
		logs.Log.Informational("[SYS] [%v]Setting proxy IP failed, no proxy available IP", key)
		return
	}
	curProxy = proxyForHost.proxys[proxyForHost.curIndex]
	proxyForHost.curIndex = (proxyForHost.curIndex+ 1) % proxyForHost.Len()
	if proxyForHost.isEcho {
		logs.Log.Informational("[SYS] Set the proxy IP as [%v](%v)",
			curProxy,
			proxyForHost.timedelay[proxyForHost.curIndex],
		)
		proxyForHost.isEcho = false
	}
	return
}

// 测试并排序
func (self *Proxy) testAndSort(key string, testHost string) (*ProxyForHost, bool) {
	logs.Log.Informational("[SYS] [%v] Testing and sorting agent IP", key)
	proxyForHost := self.usable[key]
	proxyForHost.proxys = []string{}
	proxyForHost.timedelay = []time.Duration{}
	proxyForHost.curIndex = 0
	for proxy, online := range self.all {
		if !online {
			continue
		}
		self.threadPool <- true
		go func(proxy string) {
			alive, timedelay := self.findUsable(proxy, testHost)
			if alive {
				proxyForHost.Mutex.Lock()
				proxyForHost.proxys = append(proxyForHost.proxys, proxy)
				proxyForHost.timedelay = append(proxyForHost.timedelay, timedelay)
				proxyForHost.Mutex.Unlock()
			}
			<-self.threadPool
		}(proxy)
	}
	for len(self.threadPool) > 0 {
		time.Sleep(0.2e9)
	}
	if proxyForHost.Len() > 0 {
		sort.Sort(proxyForHost)
		logs.Log.Informational("[SYS] [%v]Test and sort agent IP is completed, available：%v", key, proxyForHost.Len())
		return proxyForHost, true
	}
	logs.Log.Informational("[SYS] [%v]Test and sort agent IP is done, there is no available agent IP", key)
	return proxyForHost, false
}

// 测试代理ip可用性
func (self *Proxy) findUsable(proxy string, testHost string) (alive bool, timedelay time.Duration) {
	t0 := time.Now()
	req := &request.Request{
		Url:         testHost,
		Method:      "HEAD",
		Header:      make(http.Header),
		DialTimeout: time.Second * time.Duration(DAIL_TIMEOUT),
		ConnTimeout: time.Second * time.Duration(CONN_TIMEOUT),
		TryTimes:    TRY_TIMES,
	}
	req.SetProxy(proxy)
	_, err := self.surf.Download(req)
	return err == nil, time.Since(t0)
}
