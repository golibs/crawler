package pholcus_lib

import (
	_ "gitlab.com/golibs/crawler/example/IJGUC"
	_ "gitlab.com/golibs/crawler/example/alibaba"
	_ "gitlab.com/golibs/crawler/example/baidunews"
	_ "gitlab.com/golibs/crawler/example/baidusearch"
	_ "gitlab.com/golibs/crawler/example/car_home"
	_ "gitlab.com/golibs/crawler/example/chinanews"
	_ "gitlab.com/golibs/crawler/example/fang_resell_list"
	_ "gitlab.com/golibs/crawler/example/filetest"
	_ "gitlab.com/golibs/crawler/example/ganji_gongsi"
	_ "gitlab.com/golibs/crawler/example/googlesearch"
	_ "gitlab.com/golibs/crawler/example/hollandandbarrett"
	_ "gitlab.com/golibs/crawler/example/jdsearch"
	_ "gitlab.com/golibs/crawler/example/jingdong"
	_ "gitlab.com/golibs/crawler/example/kaola"
	_ "gitlab.com/golibs/crawler/example/lewa"
	_ "gitlab.com/golibs/crawler/example/miyabaobei"
	_ "gitlab.com/golibs/crawler/example/people"
	_ "gitlab.com/golibs/crawler/example/qq_avatar"
	_ "gitlab.com/golibs/crawler/example/shunfenghaitao"
	_ "gitlab.com/golibs/crawler/example/taobao"
	_ "gitlab.com/golibs/crawler/example/taobaosearch"
	_ "gitlab.com/golibs/crawler/example/wangyi"
	_ "gitlab.com/golibs/crawler/example/weibo_fans"
	_ "gitlab.com/golibs/crawler/example/zolpc"
	_ "gitlab.com/golibs/crawler/example/zolphone"
	_ "gitlab.com/golibs/crawler/example/zolslab"
)
