package main

import (
	"strings"
	"fmt"
	"gitlab.com/golibs/crawler/app/spider"
	"gitlab.com/golibs/crawler/app/downloader/request"
	"gitlab.com/golibs/crawler/common/goquery"
	"strconv"
	"net/http"
	"gitlab.com/golibs/crawler/logs"
	"gitlab.com/golibs/crawler/exec"
	"encoding/json"
	"gitlab.com/golibs/crawler/parsers"
	"gitlab.com/golibs/crawler/app/pipeline/collector"
	"gitlab.com/golibs/crawler/runtime/cache"
)

type ProductWrapper struct {
	Products   []Product
	Categories []Category
	Shops      []Shop
}
type Category struct {
	ID       int
	ParentID int
	Name     string
	Slug     string
	Url      string
}
type Product struct {
	ID                 string
	OriginID           string
	ShopID             string
	OriginShopID       string
	ParentID           string
	CategoryID         string
	OriginParentID     string
	Name               string
	IsRoot             bool
	Captions           map[string]interface{}
	Descriptions       []string
	Images             []string
	Price              int
	URL                string
	CrawlID            string
	PriceVAT           int
	PromotionContent   string
	PromotionImagePath string
	AdsID              int
	AdsHasPromotion    bool
	AdsHasSticker      bool
	AdsIsFreeShip      bool
	AdsHasGift         bool
	URLNoDomain        string
	IsPriceWarned      bool
	TradeMarkID        int
}

type Shop struct {
	ID                                    string
	OriginID                              string
	Domain                                string
	LogoPath                              string
	Name                                  string
	Store                                 string
	Phone                                 string
	CrawlID                               string
	IsPaid                                bool
	Provinces                             string
	AvgVote                               float64
	FanPageFacebook                       string
	IsTrusted                             bool
	IsProvideCODPaymentMethod             bool
	IsProvideCreditCardPaymentMethod      bool
	CreditCardPaymentFeePercent           float64
	IsProvideBankingTransferPaymentMethod bool
	InnerCityShippingTime                 string
	InnerCityShippingCost                 bool
	InterCityShippingTime                 string
	InterCityShippingCost                 string
	IsFreeInnerCityShippingCost           bool
	IsFreeIntercityShippingCost           bool
}
type WSSGroupProducts []struct {
	MerchantID       string  `json:"MerchantId"`
	MerchantDomain   string  `json:"MerchantDomain"`
	MerchantLogoPath string  `json:"MerchantLogoPath"`
	MerchantName     string  `json:"MerchantName"`
	MerchantStore    string  `json:"MerchantStore"`
	Provins          string  `json:"Provins"`
	AvgVote          float64 `json:"AvgVote"`
	Products []struct {
		ProductID          string `json:"ProductId"`
		ProductName        string `json:"ProductName"`
		Price              int    `json:"Price"`
		DetailURL          string `json:"DetailUrl"`
		PriceVAT           int    `json:"PriceVAT"`
		DetailURLMerchant  string `json:"DetailUrlMerchant"`
		PromotionContent   string `json:"PromotionContent"`
		PromotionImagePath string `json:"PromotionImagePath"`
		AdsID              int    `json:"AdsId"`
		AdsHasPromotion    bool   `json:"AdsHasPromotion"`
		AdsHasSticker      bool   `json:"AdsHasSticker"`
		AdsIsFreeShip      bool   `json:"AdsIsFreeShip"`
		AdsHasGift         bool   `json:"AdsHasGift"`
		URLNoDomain        string `json:"UrlNoDomain"`
		IsPriceWarned      bool   `json:"IsPriceWarned"`
		TradeMarkID        int    `json:"TradeMarkId"`
	} `json:"Products"`
	URLFanpageFacebook string `json:"UrlFanpageFacebook"`
	IsTrusted          bool   `json:"IsTrusted"`
	PaymentInfo struct {
		IsProvideCODPaymentMethod            bool    `json:"IsProvideCODPaymentMethod"`
		IsProvideCreditCardPaymentMethod     bool    `json:"IsProvideCreditCardPaymentMethod"`
		CreditCardPaymentFeePercent          float64 `json:"CreditCardPaymentFeePercent"`
		IsProvideBankingTransferPamentMethod bool    `json:"IsProvideBankingTransferPamentMethod"`
	} `json:"PaymentInfo"`
	ShippingInfo struct {
		InnercityShippingTime       string `json:"InnercityShippingTime"`
		IntercityShippingTime       string `json:"IntercityShippingTime"`
		InnercityShippingCost       bool   `json:"InnercityShippingCost"`
		IsFreeInnercityShippingCost bool   `json:"IsFreeInnercityShippingCost"`
		IntercityShippingCost       string `json:"IntercityShippingCost"`
		IsFreeIntercityShippingCost bool   `json:"IsFreeIntercityShippingCost"`
	} `json:"ShippingInfo"`
	MerchantPhone  string `json:"MerchantPhone"`
	IsPaidMerchant bool   `json:"IsPaidMerchant"`
}

func init() {
	collector.DataOutput["product-output"] = func(self *collector.Collector) (err error) {

		for _, datacell := range self.DataDocker {

			dataOutput := datacell["Data"]

			productWrapper := dataOutput.(ProductWrapper)
			logs.Log.Debug("Products", productWrapper)
			//logs.Log.Debug("Shops", dataOutput["Shops"])
			//logs.Log.Debug("Categories", dataOutput["Categories"])
		}
		return nil
	}
}

var WebSoSanh = &spider.Spider{

	Name:      "WebSosanh",
	Pausetime: 1000,
	//Keyin:           spider.KEYIN,
	Limit:           spider.LIMIT,
	EnableCookie:    false,
	NotDefaultField: true,
	RuleTree: &spider.RuleTree{
		Root: func(ctx *spider.Context) {
			baseUrl := "https://websosanh.vn"
			//baseUrl := "https://websosanh.vn/dien-thoai-samsung-galaxy-j7-2016-sm-j710/707271494/so-sanh.htm"
			for i := 1; i < 1000; i++ {
				ctx.AddQueue(&request.Request{
					Url:  baseUrl,
					Rule: "RootCategory",
					Reloadable:true,
					//Rule: "Product",
				})
			}
		},

		Trunk: map[string]*spider.Rule{
			"RootCategory": {
				ParseFunc: func(ctx *spider.Context) {

					dom := ctx.GetDom()
					dom.Find("body > div.breadcrumbs.container-fluid > div > div > ul > li > a").Each(func(index int, s *goquery.Selection) {
						link, ok := s.Attr("href")
						if ok {
							ctx.AddQueue(&request.Request{
								Url:      link,
								Priority: 1000,
								Rule:     "ListProduct",
								Temp:     request.Temp{"baseUrl": link},
							})
						}
					})

				},
			},
			"ListProduct": {
				ParseFunc: func(ctx *spider.Context) {
					query := ctx.GetDom()
					query.Find("#productListByType > ul.list-item.list-product-search > li").Each(func(i int, s *goquery.Selection) {
						if parsers.ExtractInt(s.Find(".quantity p").Text()) > 1 {
							if href, ok := s.Find("h3 > a").Attr("href"); ok {
								ctx.AddQueue(&request.Request{
									Url:      href,
									Priority: 10,
									Rule:     "Product",
								})
							}
						}
					})

					currentPage := query.Find("#productListByType > ul.pagination.pull-right.mt0 > li > a.active").First().AttrOr("data-page-index", "0")
					nextPage := query.Find("#productListByType > ul.pagination.pull-right.mt0 > li > a.next").AttrOr("data-page-index", "0")

					logs.Log.Debug("[SYS] Current Page %v , Nex Page %v", currentPage, nextPage)
					if currentPage != nextPage {
						baseUrl := ctx.GetTemp("baseUrl", "").(string)
						if baseUrl != "" {
							nexUrl := strings.Replace(baseUrl, ".htm", fmt.Sprintf("?pi=%s.htm", nextPage), 1)
							logs.Log.Debug("[SYS] BaseUrl: %v , NextUrl %v", baseUrl, nexUrl)

							ctx.AddQueue(&request.Request{
								Url:      nexUrl,
								Rule:     "ListProduct",
								Priority: 1,
								Temp:     request.Temp{"baseUrl": baseUrl},
							})
						}
					}
				},
			},
			"Product": {
				ParseFunc: func(ctx *spider.Context) {
					query := ctx.GetDom()
					productName := query.Find("#left-wrap > h1").Text()
					logs.Log.Debug("Product : %v ", productName)
					var imagesLinks []string
					query.Find("#left-wrap > div > div.gallery-wrap > div.thumbnail > ul > li > a > img").Each(func(i int, selection *goquery.Selection) {
						imageLink, _ := selection.Attr("src")
						imagesLinks = append(imagesLinks, imageLink)
					})
					query.Find("#anchorComparePrice > table > tbody > tr").Each(func(i int, selection *goquery.Selection) {
						imageLink, _ := selection.Attr("src")
						imagesLinks = append(imagesLinks, imageLink)
					})

					logs.Log.Debug("[SYS] Images : %v", imagesLinks)
					var categories []Category
					parentID := -1
					query.Find("body > div.breadcrumbs.container-fluid > div > div > ul > li").Each(func(i int, selection *goquery.Selection) {
						id, _ := strconv.Atoi(selection.AttrOr("data-id", "-1"))
						categoryName := strings.TrimSpace(selection.AttrOr("data-name", ""))
						if id < 0 || categoryName == "" {
							logs.Log.Alert("Không thể lấy được data-id hoặc data-name trong Category", selection.Text())
							return
						}
						categories = append(categories, Category{
							ID:       id,
							Name:     categoryName,
							Url:      selection.Find("a").AttrOr("href", ""),
							ParentID: parentID,
						})
						parentID = id
					})

					captions := request.Temp{}
					query.Find("#anchorProperties > div.detail-wrapper > div.detail-item").Each(func(i int, selection *goquery.Selection) {
						properties := request.Temp{}
						selection.Find(".information > table > tbody > tr").Each(func(i int, selection *goquery.Selection) {
							properties[strings.TrimSpace(selection.Find(".text").Text())] = strings.TrimSpace(selection.Find(".value").Text())
						})
						captions[strings.TrimSpace(selection.Find(".caption").Text())] = properties
					})
					logs.Log.Debug("[SYS] Captions : %v", captions)

					var descriptions []string
					query.Find("#left-wrap > div > div.merchant-wrap > div.brief-desc > ul > li").Each(func(i int, selection *goquery.Selection) {
						descriptions = append(descriptions, strings.TrimSpace(selection.Text()))
					})

					logs.Log.Debug("[SYS] Descriptions : %v", descriptions)

					totalPagesText := query.Find("#anchorComparePrice > ul > li:last-child > a").AttrOr("data-page-index", "1")
					totalPages, err := strconv.Atoi(totalPagesText)
					productId := query.Find("#hfProductId").AttrOr("value", "")
					if err == nil && productId != "" {
						logs.Log.Debug("Get Compare Price %v %v", productId, totalPages)
						var shops []Shop
						var products []Product
						products = append(products, Product{
							OriginID:     productId,
							Images:       imagesLinks,
							Name:         productName,
							Captions:     captions,
							Descriptions: descriptions,
							IsRoot:       true,
						})
						for i := 1; i < totalPages; i++ {
							headers := http.Header{}
							headers.Add("Content-Type", "application/json")
							context := ctx.Download(&request.Request{
								Method:   "POST",
								Url:      "https://websosanh.vn/Product/Detail/ListComparePrice",
								PostData: "{\"productId\":\"" + productId + "\",\"regionId\":0,\"sortType\":1,\"pageIndex\":" + strconv.Itoa(i) + "}",
								Header:   headers,
							})
							result := context.ParseJson(context.GetJson().Get("Data").String())

							totalProductJson := result.Get("Products").String()
							ctx.TextToOutput(totalProductJson, "test.json")
							//provincesJson := result.Get("Provins").String()
							var wssGroupProducts WSSGroupProducts
							context.UnmarshalJson(totalProductJson, &wssGroupProducts)

							for _, group := range wssGroupProducts {
								shops = append(shops, Shop{
									ID:                                    group.MerchantID,
									IsPaid:                                group.IsPaidMerchant,
									AvgVote:                               group.AvgVote,
									IsTrusted:                             group.IsTrusted,
									Domain:                                group.MerchantDomain,
									LogoPath:                              group.MerchantLogoPath,
									Name:                                  group.MerchantName,
									Phone:                                 group.MerchantPhone,
									Store:                                 group.MerchantStore,
									Provinces:                             group.Provins,
									FanPageFacebook:                       group.URLFanpageFacebook,
									CreditCardPaymentFeePercent:           group.PaymentInfo.CreditCardPaymentFeePercent,
									IsProvideBankingTransferPaymentMethod: group.PaymentInfo.IsProvideBankingTransferPamentMethod,
									IsProvideCODPaymentMethod:             group.PaymentInfo.IsProvideCODPaymentMethod,
									IsProvideCreditCardPaymentMethod:      group.PaymentInfo.IsProvideCreditCardPaymentMethod,
									InnerCityShippingCost:                 group.ShippingInfo.InnercityShippingCost,
									InnerCityShippingTime:                 group.ShippingInfo.InnercityShippingTime,
									InterCityShippingCost:                 group.ShippingInfo.IntercityShippingCost,
									InterCityShippingTime:                 group.ShippingInfo.IntercityShippingTime,
									IsFreeInnerCityShippingCost:           group.ShippingInfo.IsFreeInnercityShippingCost,
									IsFreeIntercityShippingCost:           group.ShippingInfo.IsFreeIntercityShippingCost,
								})
								for _, product := range group.Products {
									products = append(products, Product{
										OriginShopID:       group.MerchantID,
										OriginID:           product.ProductID,
										OriginParentID:     productId,
										Name:               product.ProductName,
										URL:                product.DetailURLMerchant,
										AdsHasGift:         product.AdsHasGift,
										AdsHasPromotion:    product.AdsHasPromotion,
										AdsHasSticker:      product.AdsHasSticker,
										AdsIsFreeShip:      product.AdsIsFreeShip,
										IsPriceWarned:      product.IsPriceWarned,
										TradeMarkID:        product.TradeMarkID,
										PromotionContent:   product.PromotionContent,
										PromotionImagePath: product.PromotionImagePath,
									})
								}
							}
						}
						productWrapper := ProductWrapper{
							Products:   products,
							Shops:      shops,
							Categories: categories,
						}
						text, _ := json.Marshal(productWrapper)
						ctx.Output(productWrapper)
						logs.Log.Debug(string(text))
					} else {
						logs.Log.Warning("Wrong param for websosanh crawler :  totalPageText=", totalPagesText, ", productID=", productId)
					}
				},
			},
			"OutPut": {
				ParseFunc: func(ctx *spider.Context) {
					ctx.FileOutput()
				},
			},
		},
	},
}

func main() {

	exec.RunCustom(cache.AppConf{
		Mode:    0,
		Limit:   10,
		OutType: "product-output",
	}, WebSoSanh)
}
