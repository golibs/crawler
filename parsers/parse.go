package parsers

import (
	"regexp"
	"strconv"
)

var intRegex = regexp.MustCompile("[0-9]+")

func ExtractInt(text string) int {
	numText := string(intRegex.Find([]byte(text)))
	num, err := strconv.Atoi(numText)
	if err != nil {
		return -1
	}
	return num
}
